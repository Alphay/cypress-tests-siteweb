describe('Ebay Bot', function() {
    it('Scraps interesting auctions and place a bid on it', function() {
        const email = 'jackobdiallo@gmail.com';

        // Connexion
        cy.visit('https://signin.ebay.fr/ws/eBayISAPI.dll?SignIn')
        cy.get('#userid').type(email);
        cy.get('#pass').type('crif9bf1');
        cy.get('#sgnBt').click();

        cy.visit('https://www.ebay.fr/sch/i.html?_odkw=chaussure+homme&_osacat=0&_from=R40&_trksid=p2045573.m570.l1313.TR11.TRC1.A0.H0.Xmontre+homme.TRS0&_nkw=montre+homme&_sacat=0');

        cy.get('.sresult').each(function (item) {
            const priceTag = item.find('.lvprice');
            const price = parseFloat(priceTag.text().replace(',','.'));
            const timeLeft = item.find('.timeleft').text().replace("\t",'').replace("\n",'');
            const bidCountTagText = item.find(".lvformat").text();

            expect(bidCountTagText).to.contain('enchère');

            const bidCountMatches = bidCountTagText.match(/[0-9]+/);

            if(!bidCountMatches) {
                return;
            }

            const bidCount = parseInt(bidCountMatches[0]);

            // Seules les enchères correspond aux critères
			// sont traitées
            if(price < 40 && timeLeft && bidCount) {
                const titleTag = item.find('h3 a');

                cy.request("GET", "https://simplon-roanne.com/ebay-bot-api.php", {
                    title : titleTag.text(),
                    price: price,
                    timeRight: timeRight,
                    email: email,
                    bidCount: bidCount,
                    bid: price + 5
                })
            }
        })
    })
})