describe('Tester la barre de navigation \header', function() {
    it('finds the content "Informations légales" in header', function() {
      cy.visit('http://localhost:4200/')
      cy.scrollTo('bottom')
      cy.get('#menu-header > li:nth-child(1) > a:nth-child(1)').click()
      cy.url().should('include', '/home')  
    })
    it('finds the content "Données personnelles" in header', function() {
      cy.visit('http://localhost:4200/')
      cy.scrollTo('bottom')
      cy.get('#menu-header > li:nth-child(2) > a:nth-child(1)').click()
      cy.url().should('include', '/home')  
    })
    it('finds the content "les cookies', function() {
      cy.visit('http://localhost:4200/')
      cy.scrollTo('bottom')
      cy.get('#menu-header > li:nth-child(3) > a:nth-child(1)').click()
      cy.url().should('include', '/home')  
    })
    it('finds the content "@Africa-Bonnes-Affaires', function() {
        cy.visit('http://localhost:4200/')
        cy.scrollTo('bottom')
        cy.get('#menu-footer > li:nth-child(4) > a:nth-child(1)').click()
        cy.url().should('include', '/home')  
      })
  })