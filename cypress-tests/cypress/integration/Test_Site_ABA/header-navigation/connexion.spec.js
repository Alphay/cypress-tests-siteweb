describe('Africa Bonnes Affaires', function() {
    context('list', function(){
        beforeEach(function(){
            cy.visit('http://137.74.113.253/')
            cy.viewport(1280, 800)
        })
    })
    it('login on the Africa Good Deals website', function() {
        // const email = 'alfad@gmail.com';
          
        // Connexion
        // cy.visit('http://137.74.113.253/')
        // cy.get('.body > .__cypress-selector-playground > .__cypress-selector-playground-cover:contains("Accueil")').should('be.visible')
        // cy.get('.body > .__cypress-selector-playground > .__cypress-selector-playground-cover:contains("Mes favoris")').should('be.visible')

        cy.visit('http://137.74.113.253/user-account')

        cy.get('input[type=email]').type('alfad@gmail.com',{force: true})
        cy.get('input[type=password]').type('1234',{force: true})
        cy.get('button').type('Connexion{enter}',{force: true})
        cy.get('button[type=button]').click({force: true})
        cy.get('.log-out').contains('bonjour Alfa | Se déconnecter')
  
        cy.visit('http://137.74.113.253/');    
    })
})