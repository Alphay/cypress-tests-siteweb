describe('Test des infos dans le footer de la page d\'accueil', function() {
    it('finds the content "Informations légales" in footer', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('#menu-footer > li:nth-child(1) > a:nth-child(1)').click()
      cy.url().should('include', '/home')  
    })
    it('finds the content "Données personnelles" in footer', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('#menu-footer > li:nth-child(2) > a:nth-child(1)').click()
      cy.url().should('include', '/home')  
    })
    it('finds the content "les cookies', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('#menu-footer > li:nth-child(3) > a:nth-child(1)').click()
      cy.url().should('include', '/home')  
    })
    it('finds the content "@Africa-Bonnes-Affaires', function() {
        cy.visit('http://137.74.113.253/')
        cy.scrollTo('bottom')
        cy.get('#menu-footer > li:nth-child(4) > a:nth-child(1)').click()
        cy.url().should('include', '/home')  
      })
  })