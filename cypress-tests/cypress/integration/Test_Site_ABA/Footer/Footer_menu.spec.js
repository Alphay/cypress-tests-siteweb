/**Test the footer menu */                          
describe('Test du menu footer depuis la page d\'accueil', function() {
    it('finds the content "À PROPOS DE NOUS"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(1) > li:nth-child(1) > a:nth-child(1)')
      cy.url().should('include', '/home') 
    })

    it('finds the content "Qui sommes-nous ?"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(1) > li:nth-child(2) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })

    it('finds the content "Nous rejoindre"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(1) > li:nth-child(3) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })

    it('finds the content "CONDITIONS GÉNÉRALES"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(2) > li:nth-child(1) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })

    it('finds the content "Conditions Générales de Vente & Règles de diffusion"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(2) > li:nth-child(2) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })

    it('finds the content "Conditions générales d’utilisation"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(2) > li:nth-child(3) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })

    it('finds the content "Vie privée / données personnelles"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(2) > li:nth-child(4) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })

    it('finds the content "NOS ACTUALITÉS"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(3) > li:nth-child(1) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })

    it('finds the content "Nos actions"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(3) > li:nth-child(2) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })

    it('finds the content "Newsletter"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(3) > li:nth-child(3) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })

    it('finds the content "Bons plans"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(3) > li:nth-child(4) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })
    it('finds the content "Annuaire des professionnels"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(3) > li:nth-child(5) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })


    it('finds the content "PUBLICITÉS"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(4) > li:nth-child(1) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })

    it('finds the content "Professionnels & Particuliers"', function() {
        cy.visit('http://137.74.113.253/')
        cy.scrollTo('bottom')
        cy.get('ul.col-6:nth-child(4) > li:nth-child(2) > a:nth-child(1)').click()
        cy.url().should('include', '/home')
    })

    it('finds the content "Entrepreneurs"', function() {
        cy.visit('http://137.74.113.253/')
        cy.scrollTo('bottom')
        cy.get('ul.col-6:nth-child(4) > li:nth-child(3) > a:nth-child(1)').click()
        cy.url().should('include', '/home')
    })

    it('finds the content "NOUS CONTACTER"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(6) > li:nth-child(1) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })

    it('finds the content "Mail"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(6) > li:nth-child(2) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })

    it('finds the content "Tél"', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('ul.col-6:nth-child(6) > li:nth-child(3) > a:nth-child(1)').click()
      cy.url().should('include', '/home')
    })

})