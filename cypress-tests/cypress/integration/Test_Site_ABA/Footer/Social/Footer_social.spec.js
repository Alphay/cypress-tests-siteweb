describe('Test du l\'icone facebook dans le footer de la page d\'accueil', function() {
    it('finds the content "facebook" in footer', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('.icon-social-r > a:nth-child(2) > i:nth-child(1)').click()
      cy.url().should('include', '/home')  
    })
    it('finds the content "twitter" in footer', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('.icon-social-r > a:nth-child(3) > i:nth-child(1)').click()
      cy.url().should('include', '/home')  
    })
    it('finds the content "pinterest" in footer', function() {
      cy.visit('http://137.74.113.253/')
      cy.scrollTo('bottom')
      cy.get('.icon-social-r > a:nth-child(4) > i:nth-child(1)').click()
      cy.url().should('include', '/home')  
    })
  })